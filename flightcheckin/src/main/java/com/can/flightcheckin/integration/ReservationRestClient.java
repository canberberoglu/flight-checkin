package com.can.flightcheckin.integration;

import com.can.flightcheckin.integration.dto.Reservation;
import com.can.flightcheckin.integration.dto.ReservationUpdateRequest;

public interface ReservationRestClient {
	
	public Reservation findReservation(Long id);
	
	public Reservation updateReservation(ReservationUpdateRequest request);
	
}
