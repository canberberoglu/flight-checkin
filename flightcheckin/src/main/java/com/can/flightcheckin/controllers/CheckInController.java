package com.can.flightcheckin.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.can.flightcheckin.integration.ReservationRestClient;
import com.can.flightcheckin.integration.dto.Reservation;
import com.can.flightcheckin.integration.dto.ReservationUpdateRequest;

@Controller
public class CheckInController {
	
	@Autowired
	private ReservationRestClient reservationRestClient;
	
	@RequestMapping("/startCheckIn")
	public String showStartCheckIn() {
		return "startCheckIn";
	}
	
	@PostMapping("/startCheckIn")
	public String startCheckIn(@RequestParam("reservationId") Long reservationId, ModelMap modelMap) {
		Reservation reservation = reservationRestClient.findReservation(reservationId);
		modelMap.addAttribute("reservation", reservation);
		return "displayReservationDetails";
	}
	
	@PostMapping("/completeCheckIn")
	public String completeCheckIn(@RequestParam("reservationId") Long reservationId,
									@RequestParam("numberOfBags") int numberOfBags) {
		ReservationUpdateRequest request = new ReservationUpdateRequest();
		request.setNumberOfBags(numberOfBags);
		request.setId(reservationId);
		request.setCheckedIn(true);
		reservationRestClient.updateReservation(request);
		return "checkInConfirmation";
	}
}
